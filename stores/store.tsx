import { makeObservable, observable } from "mobx";
import moment from "moment";
import { createContext, useContext, FC, ReactNode } from "react";

const API_URL = 'https://run.mocky.io/v3/0400d601-30c7-4831-8bd7-aabdfef6e1a5';

export type IHotel = {
  name:string;
  price:string;
  city:string;
  available_on: string;
}

export type SerializedHotelsStoreData = {
  allHotels: Array<IHotel>;
  unfilteredHotels: Array<IHotel>;
  hotels: Array<IHotel>;
}


const PRICE_SORT_KET: string = 'price';
const NAME_SORT_KET: string = 'name';

class HotelsStore {
  allHotels: Array<IHotel> = [];
  unfilteredHotels: Array<IHotel> = [];
  @observable hotels: Array<IHotel> = [];

  ascending: boolean = false;

  lastItemSorted: string = PRICE_SORT_KET;

  setHotels(hotels: Array<IHotel>):void {
    this.allHotels = this.hotels = this.unfilteredHotels = hotels;
  }

  filterHotels({from, to}) {
    this.unfilteredHotels = this.allHotels.filter((hotel: IHotel) => {
      const diffFrom = moment(from).diff(moment(hotel.available_on), 'days');
      const diffTo = moment(to).diff(moment(hotel.available_on), 'days');
      return diffFrom <= 0 && diffTo >= 0;
    });
    this.hotels = this.unfilteredHotels;
  }

  filterHotelsByMaxPrice(maxPriceFilter:number) {
    if (maxPriceFilter > 0) {
      const filteredHotels: Array<IHotel> = this.unfilteredHotels.filter((hotel: IHotel) => parseInt(hotel.price) <= maxPriceFilter);
      if (filteredHotels.length != this.hotels.length) {
        this.hotels = filteredHotels;
      }
    }
  }


  get maxHotelPrice() {
    let maxPrice = -1;
    this.unfilteredHotels.forEach(hotel => {
      if (maxPrice < parseInt(hotel.price)) {
        maxPrice = parseInt(hotel.price);
      }
    })
    return maxPrice;
  }

  get minHotelPrice() {
    if (!this.unfilteredHotels[0]) return -1;
    let minPrice = parseInt(this.unfilteredHotels[0].price);
    this.unfilteredHotels.forEach(hotel => {
      if (minPrice > parseInt(hotel.price)) {
        minPrice = parseInt(hotel.price);
      }
    })
    return minPrice;
  }

  sortByPrice = () => {
    if (this.lastItemSorted != PRICE_SORT_KET) {
      this.ascending = false;
    }
    this.lastItemSorted = PRICE_SORT_KET;
    this.hotels.sort((a: IHotel, b: IHotel) => {
      return this.ascending ? (parseInt(b.price) - parseInt(a.price)) : (parseInt(a.price) - parseInt(b.price));
    });
    this.ascending = !this.ascending;
  }

  sortByName = () => {
    if (this.lastItemSorted != NAME_SORT_KET) {
      this.ascending = false;
    }
    this.lastItemSorted = NAME_SORT_KET;
    this.hotels.sort((a: IHotel, b: IHotel) => {
      if (this.ascending) {
        if (a.name > b.name) return -1;
        if (a.name < b.name) return 1;
      } else {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
      }
      return 0;
    });
    this.ascending = !this.ascending;
  }

  getSerializedData():SerializedHotelsStoreData {
    return {
      allHotels: this.allHotels,
      unfilteredHotels: this.unfilteredHotels,
      hotels: this.hotels,
    };
  }


  async fetch(query) {
    const res = await fetch(API_URL);
    const hotels = (await res.json()) as Array<IHotel>;
    this.setHotels(hotels);
    this.filterHotels(query);
  }
  


  constructor(serializedData: SerializedHotelsStoreData) {
    makeObservable(this);
    if (serializedData) {
      this.allHotels = serializedData.allHotels;
      this.hotels = serializedData.hotels;
      this.unfilteredHotels = serializedData.unfilteredHotels;
    }
  }
}

const StoreContext = createContext<HotelsStore>(undefined);

type StoreProviderProps = {
  store: HotelsStore,
  children: ReactNode,
}

const StoreProvider: FC<StoreProviderProps> = ({ store, children }: StoreProviderProps) => {
  return (
    <StoreContext.Provider value={store}>{children}</StoreContext.Provider>
  );
};

const useHotelsStore = () => {
  return useContext(StoreContext);
};

let clientSideStore;

const getHotelsStore = (initialData: SerializedHotelsStoreData = null) => {
  if (typeof window === 'undefined') {
    return new HotelsStore(initialData);
  }
  if (!clientSideStore) {
    clientSideStore = new HotelsStore(initialData);
  }
  return clientSideStore;
}

export { HotelsStore, StoreProvider, useHotelsStore, getHotelsStore };
