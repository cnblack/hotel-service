import App from "next/app";
import { getHotelsStore, SerializedHotelsStoreData, StoreProvider } from "../stores/store";
import '../styles/globals.css';

function MyApp({ Component, pageProps, initialData }) {

  const store = getHotelsStore(initialData);

  return (
    <StoreProvider store={store}>
      <Component {...pageProps} />
    </StoreProvider>
  );
}

MyApp.getInitialProps = async (appConext) => {
  const hotelsStore = getHotelsStore();

  // to share hotelsStore for all .getInitialProps across the different pages
  appConext.ctx.hotelsStore = hotelsStore;

  const appProps = await App.getInitialProps(appConext);

  const initialData: SerializedHotelsStoreData = hotelsStore.getSerializedData();

  return {
    ...appProps,
    initialData
  }
}

export default MyApp;
