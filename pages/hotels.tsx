import { action } from 'mobx';
import { observer } from 'mobx-react-lite';
import { NextPage } from 'next';
import React, { useState, useMemo } from 'react';
import { useHotelsStore, IHotel, HotelsStore } from '../stores/store';
import moment from 'moment';
import Hotel from '../components/Hotel';
import { useRouter } from 'next/router'
import HotelPriceFilter from '../components/HotelPriceFilter';
import Button from '../components/Button';
import Layout from '../components/Layout';
import ErrorLayout from '../components/ErrorLayout';
import { t } from '../common/local';
import SearchInput from '../components/SearchInput';

const getNights = () => {
  const router = useRouter();
  const {to, from} = router.query;
  return useMemo(() => moment(to).diff(moment(from), 'days'), []);
}

const canUserVisit = (): string => {
  const router = useRouter();
  const store = useHotelsStore();

  if (!router.query.from || !router.query.to) {
    return t('error_occured');
  }

  if (moment(router.query.to).diff(moment(router.query.from)) < 0) {
    return t('date_range_invalid');
  }

  if (store.hotels.length == 0) {
    return t('no_hotels')
  }

  return null;
}

const getHotels = (searchNameQuery: string, store: HotelsStore): Array<IHotel> => {
  return searchNameQuery.trim() != '' ?
    store.hotels.filter(hotel => hotel.name.toLowerCase().includes(searchNameQuery.toLowerCase())) :
    store.hotels
}


const HotelsPage: NextPage<{}> = observer(() => {
  const store = useHotelsStore();
  const nights = getNights();
  const [searchNameQuery, setSearchNameQuery] = useState<string>('');

  const errorMessage = canUserVisit();
  if (errorMessage) {
    return (
      <ErrorLayout>
        {errorMessage}
      </ErrorLayout>
    );
  }

  const hotels = getHotels(searchNameQuery, store); 

  return (
    <Layout>
      <div className='flex flex-row justify-end'>
        <div className='w-3/4 flex flex-row justify-between'>
          <p className="text-xl">{t('total_nights')}: {nights}</p>
          <div>
            <Button dataId="sort_by_price" onClick={action(store.sortByPrice)}>{t('sort_by_price')}</Button>
            <Button dataId="sort_by_name" onClick={action(store.sortByName)}>{t('sort_by_name')}</Button>
          </div>
        </div>
      </div>
      <div className='flex'>
        <div className='w-1/4'>
          <SearchInput placeholder={t('hotel_name')} value={searchNameQuery} setValue={setSearchNameQuery} />
          <HotelPriceFilter nights={nights} />
        </div>
        <div className='hotels-container w-3/4 flex justify-center items-center flex-wrap'>
          {
            hotels.length > 0 ?
              hotels.map((hotel: IHotel) => <Hotel key={hotel.name} hotel={hotel} nights={nights} />) :
              t('no_hotels')
          }
        </div>
      </div>
    </Layout>
  );
});

HotelsPage.getInitialProps = async ({ hotelsStore, query }) => {
  await hotelsStore.fetch(query);
  return {
    date: `${query.from}_${query.to}`,
  };
}

export default HotelsPage;