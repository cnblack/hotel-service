import { NextPage } from 'next';
import React, { useCallback, useState } from 'react';
import { useRouter } from 'next/router'
import Layout from '../components/Layout';
import Button from '../components/Button';
import DateInput from '../components/DateInput';
import moment from 'moment';
import { t } from '../common/local';


const isEmpty = (value: string):boolean => !value || value === '';

const validateData = ({fromDate, toDate}) => {
  const _errors = {};
  if (isEmpty(fromDate)) {
    _errors['fromDate'] = {message: t('field_empty')};
  }
  if (isEmpty(toDate)) {
    _errors['toDate'] = {message: t('field_empty')};
  }
  if (moment(toDate).diff(moment(fromDate)) < 0) {
    _errors['toDate'] = {message: t('date_range_invalid')};
  }
  return Object.keys(_errors).length > 0 ? _errors : false;
}

const IndexPage: NextPage<{}> = () => {
  const [fromDate, setFromDate] = useState<string>('');
  const [toDate, setToDate] = useState<string>('');
  const [errors, setErrors] = useState({})
  const router = useRouter();

  const handleSearchQuery = useCallback(() => {
    const errors = validateData({ fromDate, toDate });
    if (errors) {
      setErrors(errors);
    } else {
      router.push(`/hotels?from=${fromDate}&to=${toDate}`);
    }
  }, [fromDate, toDate]);

  return (
    <Layout>
      <div className='flex flex-row justify-center text-xl'>
        <DateInput label={t('from')} date={fromDate} setDate={setFromDate} error={errors['fromDate']} />
        <DateInput label={t('to')} date={toDate} setDate={setToDate} error={errors['toDate']} />
        <div className='p-2'>
          <Button dataId='search' onClick={handleSearchQuery}>{t('search')}</Button>
        </div>
      </div>
    </Layout>
  )
};


export default IndexPage;