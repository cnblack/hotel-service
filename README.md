node version v12.22.9  
npm version v6.14.15

### technologies used:
 - NextJS
 - TypeScript
 - MobX
 - Cypress (for testing)
 - Tailwind (css)

you first need to run `npm install` in order to setup the app

to run app in dev mode `npm run dev`

to build app `npm run build`

to run test headless `npm run cypress:headless`

to run cypress testing and see test results `npm run cypress`


hotels are availble between `2022-08-15` to `2022-10-27`