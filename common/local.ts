const Localization = {
  locales: {
    'en': {
      'from': 'From',
      'to': 'To',
      'search': 'Search',
      'home': 'Home',
      'total_nights': 'Total Nights',
      'sort_by_price': 'Sort By Price',
      'sort_by_name': 'Sort By Name',
      'hotel_name': 'Hotel Name',
      'price_filter': 'Price Filter',
      'name':'Name',
      'price':'Price',
      'city':'City',
      'field_empty': 'please fill the following field',
      'date_range_invalid': 'the date range is invalid',
      'no_hotels': 'No Hotels were found',
      'error_occured': 'an error occured please check your date inputs',
    },
  },
  currentLanguage: 'en',
  CURRENCY: 'AED',
}

const t = (key) => {
  return Localization.locales[Localization.currentLanguage][key];
}

export { t, Localization };