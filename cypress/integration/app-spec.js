describe('Navigation', () => {
  it('should navigate to the hotel', () => {
    cy.visit('http://localhost:3000/')

    cy.get('#From').type('2022-09-01')
    cy.get('#To').type('2022-10-01')
    cy.get('[data-id=search]').click();

    cy.url().should('include', '/hotels?from=2022-09-01&to=2022-10-01');
  });

  it('should show error on wrong url', () => {
    cy.visit('http://localhost:3000/hotels')

    cy.get('.error-layout').should('be.visible');

  })
})

describe('Form Validation', () => {
  it('should show error if no input is filled', () => {
    cy.visit('http://localhost:3000/');

    cy.get('[data-id=search]').click();

    cy.get('.error-message').should('be.visible');
  })

  it('should show error if dates are reversed', () => {
    cy.visit('http://localhost:3000/');


    cy.get('#To').type('2022-09-01')
    cy.get('#From').type('2022-10-01')
    cy.get('[data-id=search]').click();

    cy.get('.error-message').should('be.visible');
  })
})

describe('Hotel Listing', () => {
  it('should show only one search item for Ramada', () => {
    cy.visit('http://localhost:3000/')

    cy.get('#From').type('2022-09-01')
    cy.get('#To').type('2022-10-01')
    cy.get('[data-id=search]').click();

    cy.url().should('include', '/hotels?from=2022-09-01&to=2022-10-01');

    cy.get('.search-input').should('be.visible');

    cy.get('.search-input').type('Ramada');

    cy.get('.hotels-container').children().should('have.length', 1)
  });

  it('should show no results', () => {
    cy.visit('http://localhost:3000/')

    cy.get('#From').type('2022-09-01')
    cy.get('#To').type('2022-10-01')
    cy.get('[data-id=search]').click();

    cy.url().should('include', '/hotels?from=2022-09-01&to=2022-10-01');

    cy.get('.search-input').should('be.visible');

    cy.get('.search-input').type('hzdujasdnasbnduasdbnuyasudbuas');

    cy.get('.hotels-container').contains('No Hotels were found');
  })

  it('should sort based on name', () => {
    cy.visit('http://localhost:3000/hotels?from=2022-08-01&to=2022-11-01');

    cy.get('[data-id=sort_by_name]').click();

    cy.get('.hotel-name').then(items => {
      const unsortedItems = items.map((_, html) => Cypress.$(html).text()).get();
      const sortedItems = unsortedItems.slice().sort();
      expect(unsortedItems, 'Items are sorted').to.deep.equal(sortedItems);
    });

  })

  it('should sort based on price', () => {
    cy.visit('http://localhost:3000/hotels?from=2022-08-01&to=2022-11-01');

    cy.get('[data-id=sort_by_price]').click();

    cy.get('.hotel-price').then(items => {
      const unsortedItems = items.map((_, html) => Cypress.$(html).text()).get();
      const sortedItems = unsortedItems.slice().sort();
      expect(unsortedItems, 'Items are sorted').to.deep.equal(sortedItems);
    });

  })
})