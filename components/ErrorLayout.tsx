
import Link from 'next/link';
import { FC, ReactNode } from 'react';
import { t } from '../common/local';

type Props = {
  children: ReactNode
}

const ErrorLayout: FC<Props> = ({children}: Props) => {
  return (
    <div className="error-layout bg-red-600 text-white text-3xl w-screen h-screen flex justify-center items-center">
      {children}
      <Link href="/"><a className='text-blue-300 px-1'>{t('home')}</a></Link>
    </div>
  );
}

export default ErrorLayout;