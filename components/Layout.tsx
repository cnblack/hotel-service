import { FC, ReactNode } from "react";

type Props = {
  children: ReactNode
}


const Layout: FC<Props> = ({ children }: Props) => {
  return (
    <div className="container mx-auto p-5">
      {children}
    </div>
  )
}

export default Layout;