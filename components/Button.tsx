import { FC, MouseEventHandler, ReactNode } from "react";

type Props = {
  onClick: MouseEventHandler<HTMLButtonElement>,
  children: ReactNode,
  dataId: string
}

const Button: FC<Props> = ({onClick, children, dataId}: Props) => {
  return (
    <button
      onClick={onClick}
      data-id={dataId}
      className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-xl px-5 py-2.5 text-center mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
    >
      {children}
    </button>
  )
}

export default Button;