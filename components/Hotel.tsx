import { FC } from 'react';
import { IHotel } from '../stores/store';
import { t, Localization } from '../common/local';

type Props = {
  hotel: IHotel;
  nights: number;
}

const Hotel: FC<Props> = ({ hotel, nights }: Props) => {
  return (
    <div className='rounded-lg container mx-auto w-2/5 bg-blue-900 text-white m-3 p-5'>
      <div>{t('name')}: <span className='hotel-name'>{hotel.name}</span></div>
      <div>{t('price')}: <span className='hotel-price'>{parseInt(hotel.price) * nights}</span> {Localization.CURRENCY}</div>
      <div>{t('city')}: {hotel.city}</div>
    </div>
  );
}

export default Hotel;