import { FC } from "react";

type Props = {
  error: {message: string};
  date: string;
  setDate: Function;
  label: string;
}

const DateInput: FC<Props> = ({ error, date, setDate, label }: Props) => {
  return (
    <div className='p-2 flex '>
      <div className="mr-3 p-2">
        {label}:
      </div>
      <div>
        <input id={label} className={`rounded border ${error ? 'border-red-600' :'border-blue-900'} p-2`} type='date' value={date} onChange={(e) => setDate(e.target.value)} />
        {!!error && <p className="error-message text-xs text-red-600 text-center">{error.message}</p>}
      </div>
    </div>
  )
}

export default DateInput;