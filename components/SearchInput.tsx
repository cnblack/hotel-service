import { FC, useCallback } from "react";

type Props = {
  value: string;
  setValue: Function;
  placeholder: string;
}

const SearchInput: FC<Props> = ({ value, setValue, placeholder }: Props) => {
  const handleTextChange = useCallback((e) => {
    setValue(e.target.value)
  }, [])
  return (
    <input
      placeholder={placeholder}
      value={value}
      onChange={handleTextChange}
      type="text"
      className='search-input rounded border border-blue-600 p-2 mb-3'
    />
  )
}

export default SearchInput;