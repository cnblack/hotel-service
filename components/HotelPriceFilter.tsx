import { action } from 'mobx';
import React, { FC, useCallback, useState } from 'react';
import { useHotelsStore } from '../stores/store';
import { t, Localization } from '../common/local';

type Props = {
  nights: number,
}

const HotelPriceFilter: FC<Props> = ({ nights }:Props) => {
  const store = useHotelsStore();
  const [priceFilter, setPriceFilter] = useState<number>(store.maxHotelPrice);

  const handlePriceChange = useCallback(action((e) => {
    const newPriceFilter = parseInt(e.target.value);
    setPriceFilter(newPriceFilter);
    store.filterHotelsByMaxPrice(newPriceFilter);
  }), []);

  return (
    <div className='text-xl'>
      <p>{t('price_filter')} <span className='text-blue-700'>{priceFilter * nights} {Localization.CURRENCY}</span></p>
      <input
        type="range"
        min={store.minHotelPrice}
        max={store.maxHotelPrice}
        value={priceFilter}
        onChange={handlePriceChange}
        title={`${priceFilter * nights}`}
      />
    </div>
  );
}

export default HotelPriceFilter;